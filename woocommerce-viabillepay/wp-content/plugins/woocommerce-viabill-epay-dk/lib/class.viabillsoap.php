<?php

class viabillsoap
{	
	private $pwd = "";
	private $client = null;
	
	function __construct($pwd = "", $subscription = false)
	{
		if($subscription)
			$client = new SoapClient('https://ssl.ditonlinebetalingssystem.dk/remote/subscription.asmx?WSDL');
		else
			$client = new SoapClient('https://ssl.ditonlinebetalingssystem.dk/remote/payment.asmx?WSDL');
		
		$this->pwd = $pwd;
		
		$this->client = $client;
	}
	
	public function authorize($merchantnumber, $subscriptionid, $orderid, $amount, $currency, $instantcapture, $group, $email)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["subscriptionid"] = $subscriptionid;
		$vb_epay_params["orderid"] = $orderid;
		$vb_epay_params["amount"] = $amount;
		$vb_epay_params["currency"] = $currency;
		$vb_epay_params["instantcapture"] = $instantcapture;
		$vb_epay_params["group"] = $group;
		$vb_epay_params["email"] = $email;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["fraud"] = 0;
		$vb_epay_params["transactionid"] = 0;
		$vb_epay_params["pbsresponse"] = "-1";
		$vb_epay_params["epayresponse"] = "-1";
		
		$result = $this->client->authorize($vb_epay_params);
		
		if($result->authorizeResult == true)
			return true;
		else
		{
			if($result->epayresponse != "-1")
				return new WP_Error('broke', $this->getEpayError($merchantnumber, $result->epayresponse));
			elseif($result->pbsresponse != "-1")
				return new WP_Error('broke', $this->getPbsError($merchantnumber, $result->pbsresponse));
			else
				return new WP_Error('broke', 'An unknown error occured');
		}
	}
	
	public function deletesubscription($merchantnumber, $subscriptionid)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["subscriptionid"] = $subscriptionid;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["epayresponse"] = "-1";
		
		$result = $this->client->deletesubscription($vb_epay_params);
		
		if($result->deletesubscriptionResult == true)
			return true;
		else
		{
			if($result->epayresponse != "-1")
				return new WP_Error('broke', $this->getEpayError($merchantnumber, $result->epayresponse));
			else
				return new WP_Error('broke', 'An unknown error occured');
		}	
	}
	
	public function capture($merchantnumber, $transactionid, $amount)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["transactionid"] = $transactionid;
		$vb_epay_params["amount"] = $amount;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["pbsResponse"] = "-1";
		$vb_epay_params["epayresponse"] = "-1";
		
		$result = $this->client->capture($vb_epay_params);
		
		if($result->captureResult == true)
			return true;
		else
		{
			if($result->epayresponse != "-1")
				return new WP_Error('broke', $this->getEpayError($merchantnumber, $result->epayresponse));
			elseif($result->pbsResponse != "-1")
				return new WP_Error('broke', $this->getPbsError($merchantnumber, $result->pbsResponse));
			else
				return new WP_Error('broke', 'An unknown error occured');
		}
	}
	
	public function moveascaptured($merchantnumber, $transactionid)
	{
		return new WP_Error('broke', 'An unknown error occured');
		
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["transactionid"] = $transactionid;
		$vb_epay_params["epayresponse"] = "-1";
		$vb_epay_params["pwd"] = $this->pwd;
		
		$result = $this->client->move_as_captured($vb_epay_params);
		
		if($result->move_as_capturedResult == true)
			return true;
		else
		{
			if($result->epayresponse != "-1")
				return new WP_Error('broke', $this->getEpayError($merchantnumber, $result->epayresponse));
			else
				return new WP_Error('broke', 'An unknown error occured');
		}
	}
	
	public function credit($merchantnumber, $transactionid, $amount)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["transactionid"] = $transactionid;
		$vb_epay_params["amount"] = $amount;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["epayresponse"] = "-1";
		$vb_epay_params["pbsresponse"] = "-1";
		
		$result = $this->client->credit($vb_epay_params);
		
		if($result->creditResult == true)
			return true;
		else
		{
			if($result->epayresponse != "-1")
				return new WP_Error('broke', $this->getEpayError($merchantnumber, $result->epayresponse));
			elseif($result->pbsresponse != "-1")
				return new WP_Error('broke', $this->getPbsError($merchantnumber, $result->pbsresponse));
			else
				return new WP_Error('broke', 'An unknown error occured');
		}
	}
	
	public function delete($merchantnumber, $transactionid)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["transactionid"] = $transactionid;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["epayresponse"] = "-1";
		
		$result = $this->client->delete($vb_epay_params);
		
		if($result->deleteResult == true)
			return true;
		else
		{
			if($result->epayresponse != "-1")
				return new WP_Error('broke', $this->getEpayError($merchantnumber, $result->epayresponse));
			else
				return new WP_Error('broke', 'An unknown error occured');
		}
	}
	
	public function getEpayError($merchantnumber, $epay_response_code)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["language"] = 2;
		$vb_epay_params["epayresponsecode"] = $epay_response_code;
		$vb_epay_params["epayresponse"] = "-1";
		
		$result = $this->client->getEpayError($vb_epay_params);
		
		if($result->getEpayErrorResult == "true")
			return new WP_Error('broke', $result->epayresponsestring);
		else
			return new WP_Error('broke', 'An unknown error occured');
		
	}
	
	public function getPbsError($merchantnumber, $pbs_response_code)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["language"] = 2;
		$vb_epay_params["pbsresponsecode"] = $pbs_response_code;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["epayresponse"] = "-1";
		
		$result = $this->client->getPbsError($vb_epay_params);
		
		if($result->getPbsErrorResult == "true")
			return new WP_Error('broke', $result->pbsresponsestring);
		else
			return new WP_Error('broke', 'An unknown error occured');
	}
	
	public function gettransaction($merchantnumber, $transactionid)
	{
		$vb_epay_params = array();
		$vb_epay_params["merchantnumber"] = $merchantnumber;
		$vb_epay_params["transactionid"] = $transactionid;
		$vb_epay_params["pwd"] = $this->pwd;
		$vb_epay_params["epayresponse"] = "-1";
		
		$result = $this->client->gettransaction($vb_epay_params);
		
		if($result->gettransactionResult == true)
			return $result;
		else
		{
			if($result->epayresponse != "-1")
				return new WP_Error('broke', $this->getEpayError($merchantnumber, $result->epayresponse));
			else
				return new WP_Error('broke', 'An unknown error occured');
		}
	}
}
?>