<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<li class="wc_payment_method payment_method_<?php echo $gateway->id; ?>">
	<input id="payment_method_<?php echo $gateway->id; ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />
<!-- /////////////////////         code for viabill start    /////////////////////////////////// -->

	<?php if($gateway->id =="viabill_epay_dk"){ 
		global $woocommerce;
 		 $total =  $woocommerce->cart->total;
	 ?>
		 
			<label for="payment_method_<?php echo $gateway->id; ?>">
					
					<span id="vbdefaulttitle">
						<div class="ViaBill_pricetag_payment" style="display:inline-flex;" price="<?php echo $total; ?>">
							<?php echo $gateway->get_title(); ?>
						</div>
						<?php echo $gateway->get_icon(); ?>
					</span>
					 


			</label>
	<?php  }
	else {
		?>
		<label for="payment_method_<?php echo $gateway->id; ?>">
		<?php echo $gateway->get_title(); ?> <?php echo $gateway->get_icon(); ?>
		</label>
		<?php } //end of else ?>

	<!-- ////////////////////////////////   code for viabill end          /////////////////////////////////////////////////////////////// -->

	<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
		<div class="payment_box payment_method_<?php echo $gateway->id; ?>" <?php if ( ! $gateway->chosen ) : ?>style="display:none;"<?php endif; ?>>
			<?php $gateway->payment_fields(); ?>
		</div>
	<?php endif; ?>
</li>


<!-- ////////////////////////////////   code for viabill start          /////////////////////////////////////////////////////////////// -->

<!-- Script for ViaBill Pricetag -->
<?php  if($gateway->vbpricetag){ ?>
<script type="text/javascript">	
<?php  echo $gateway->vbpricetag; ?>

var title = 'ViaBill';

var priceqp = "<?php echo  $total; ?>";  
if(vb.isLow(priceqp)  || vb.isHigh(priceqp)){				
	}
else{				
jQuery('#vbdefaulttitle').html(title);	
}		
</script>
<?php } ?>
<!-- ////////////////////////////////   code for viabill end          /////////////////////////////////////////////////////////////// -->