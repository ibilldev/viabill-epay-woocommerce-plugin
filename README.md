### WooCommerce : ViaBill ePay Module ###
----------------------  

ViaBill has developed a free payment module using ePay Payment Gateway which enables your customers to pay online for their orders in your WooCommerce solution for WordPress.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
----
-- version: 1.1
- Plugin on BitBucket (https://bitbucket.org/ibilldev/viabill-epay-woocommerce-plugin/)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your Woocommerce Web Shop to provide a separate payment option to pay using ViaBill.

#Requirements
------------
* PHP >= 5.2.0

#Compatibility
-------------
* Wordpress >= 4.3
* Woocommerce >=2.3.0


###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 

2. Module contains one folder a.) wp-content

Please follow the following folder structure to place the relevant files for ViaBill Integration:

![woocom-epay-structure.PNG](https://bitbucket.org/repo/R4jnBj/images/3805162802-woocom-epay-structure.PNG)

3. Within the 'wp-content/plugins' folder, place the folder named 'woocommerce-viabill-epay-dk'

4. Next, Modification : make sure your apply all modifications - marked with "code for viabill "

				 wp-content/plugins/woocommerce/templates/checkout/payment-method.php
				

5. Go to the Admin-> Plugins-> Inactive. Look for "WooCommerce ViaBill ePay Payment Solutions Gateway"

6. Click on Activate.

7. Now click on Woocommerce -> Settings -> Checkout : ViaBill

8. Enable/Disable :  Please Enable ViaBill ePay. 

9. Enter Merchant Number 

10. Save and Done.


##Uninstallation/Disable Module/Delete Module
-----------------------
1. Go to the Admin->Plugins-> Active
2. Look For "WooCommerce ViaBill ePay Payment Solutions Gateway"
3. Choose Option to Deactivate
4. Now You get the option to Delete the Plugin.



#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)
